package Controlador;
import Modelo.Cotizacion;
import Vista.dlgCotizacion;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;



public class Controlador implements ActionListener{
    
    private Cotizacion cot;
    private dlgCotizacion vista;
    
    
    public Controlador(Cotizacion cot, dlgCotizacion vista){
        
        this.cot = cot;
        this.vista = vista;
        
        //ESCUCHAR BOTONES
        vista.btnNuevo.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnCancel.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        
        //COMBO BOX
        vista.cbOpcion.addActionListener(this);
        
        
    }
    
    
    //METODOLIMPIAR
    public void limpiar(){
        
        vista.txtDescripcion.setText("");
        vista.txtNumeroC.setText("");
        vista.txtPlazo.setText("");
        vista.txtPorcentaje.setText("");
        vista.txtPrecio.setText("");
        
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
       
        //COMBO BOX MESES
        if(e.getSource()==vista.cbOpcion){
            vista.txtPlazo.setText(vista.cbOpcion.getSelectedItem().toString());
            
        }
        
        //BOTON LIMPIAR
        if(e.getSource()==vista.btnLimpiar){
            
            limpiar();
            
        }
        
        // BOTON NUEVO
        if(e.getSource()==vista.btnNuevo){
            
            //ACTIVAR TXTF
            vista.txtNumeroC.setEnabled(true);
            vista.txtDescripcion.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.txtPorcentaje.setEnabled(true);

            
            
            //ACTIVAR BOTONES
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            
        }
        
        if(e.getSource()==vista.btnMostrar){
            
            //MOSTRAR VALORES EN EL OBJETO
            vista.txtNumeroC.setText(String.valueOf(cot.getNumeroC()));
            vista.txtDescripcion.setText(String.valueOf(cot.getDescripcion()));
            vista.txtPrecio.setText(String.valueOf(cot.getPrecio()));
            vista.txtPorcentaje.setText(String.valueOf(cot.getPagoInicial()));
            vista.txtPlazo.setText(String.valueOf(cot.getPlazo()));
            
            vista.txtPagoInicial.setText(String.valueOf(cot.calcularPagoInicial()));
            vista.txtTotal.setText(String.valueOf(cot.calcularTotal()));
            vista.txtPagoM.setText(String.valueOf(cot.calcularPagoMensual()));
            
            
        }
        
        //BOTON GUARDAR
        
        if(e.getSource()==vista.btnGuardar){
            
            cot.setDescripcion(vista.txtDescripcion.getText());
           try{
                cot.setNumeroC(Integer.parseInt(vista.txtNumeroC.getText()));
                cot.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                cot.setPagoInicial(Integer.parseInt(vista.txtPorcentaje.getText()));
                cot.setPlazo(Integer.parseInt(vista.txtPlazo.getText()));
                
                JOptionPane.showMessageDialog(vista, "Se agregó existosamente");

           } catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
            }
             catch(Exception ex2){
                
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex2.getMessage());
            }
           
        }
        
        //BOTON CANCELAR
        if(e.getSource()==vista.btnCancel){
            
            //DESACTIVAR BOTONES Y TXTF
            vista.txtNumeroC.setEnabled(false);
            vista.txtDescripcion.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.txtPorcentaje.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            
        }
        
        if(e.getSource()==vista.btnCerrar){
            int option = JOptionPane.showConfirmDialog(vista, "¿Desea salir?",
                    "Seleccione", JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
        
    }
    
    
    //INICIALIZANDO VISTA 
    private void iniciarVista(){
        
        vista.setTitle(": :     COTIZACIÓN       : :");
        vista.setSize(1600,500);
        vista.setVisible(true);
    }
    
    

    public static void main(String[] args) {
        
        Cotizacion cot = new Cotizacion();
        dlgCotizacion vista = new dlgCotizacion(new JFrame(), true);
        
        Controlador contra = new Controlador(cot, vista);
        contra.iniciarVista();
    }

    
}