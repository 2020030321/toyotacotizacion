
package Modelo;


public class Cotizacion {
    
    
    private int numeroC, pagoI, plazo;
    private String descripcion;
    private float precio;
    
    
    //CONSTRUCTOR CAPTURAR VALORES
    public Cotizacion(int nc, String des, float pre, int pi, int pla){}
    
    //CONSTRUCTOR CLONAR OBJETO
    public Cotizacion(Cotizacion cot){
        
        this.numeroC = cot.numeroC;
        this.descripcion = cot.descripcion;
        this.precio = cot.precio;
        this.pagoI = cot.pagoI;
        this.plazo = cot.plazo;
        
    }
    
    //CONSTRUCTOR OBJETO VACIO
    public Cotizacion(){}
    
    public void setNumeroC(int nc){
        this.numeroC = nc;
        
    }
    
    public void setDescripcion(String des){
        this.descripcion = des;
    }
    
    public void setPrecio(float pre){
        this.precio = pre;
    }
    
    public void setPagoInicial(int pi){
        this.pagoI = pi;
    }
    
    public void setPlazo(int pla){
        this.plazo = pla;
    }
    
    public int getNumeroC(){
        return numeroC;
    }
    
    public String getDescripcion(){
        return descripcion;
    }
    
    public float getPrecio(){
        return precio;
    }
    
    public int getPagoInicial(){
        return pagoI;
    }
    
    public int getPlazo(){
        return plazo;
    }
    
    public float calcularPagoInicial(){
        return precio * ((float)pagoI / 100);
    }
    
       
    public float calcularTotal(){
        return (precio-calcularPagoInicial());
    }
   
    public float calcularPagoMensual(){
        return(calcularTotal()/plazo);
    }
    
    
}
